# Copyright (c) 2015 University of Cantabria.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Authors: Ivan Perez

# This topology is only compatible with the booksim network

from m5.params import *
from m5.objects import *

from BaseTopology import SimpleTopology

class PythiumMars(SimpleTopology):
    description='Pythium Mars mem. architecture'

    def makeTopology(self, options, network, IntLink, ExtLink, Router):

        # Split the list of nodes in four lists, each one for each
        # type of node
        l1cache_nodes = []
        l2cache_nodes = []
        dir_nodes = []
        dma_nodes = []
        for node in self.nodes:
            if node.type == "L1Cache_Controller":
                l1cache_nodes.append(node)
            elif node.type == "L2Cache_Controller":
                l2cache_nodes.append(node)
            elif node.type == "Directory_Controller":
                dir_nodes.append(node)
            elif node.type == "DMA_Controller":
                dma_nodes.append(node)

        # Pythium Mars has 64 cores (64 L1 controllers),
        # 16 L2 controllers, 16 Memory controllers,
        # and 2 DMA controllers
        # (seems that the default config defines 3 for Ruby)
        assert(len(l1cache_nodes) == 64)
        assert(len(l2cache_nodes) == 16)
        assert(len(dir_nodes) == 16)
        assert(len(dma_nodes) == 3)

        # Pythium Mars has routers of 6 ports, with 4 injectors in two
        # opossite corners, and 3 injectors in the rest.
        # If we consider the Ruby switches as NICs it has 26 "usefull"
        # routers and 6 "dummies". We need these 6 dummy routers
        # to map the injectors correctly with the booksim topology.
        routers = [Router(router_id=i) for i in range(32)]
        network.routers = routers

        # The first 16 ID Routers (ID NICs -> Booksim node ID) are for 4 l1
        # controllers with one l2 controller.
        ext_links = []
        link_id_next = 0
        for i in range(16): # i is the router ID

            ext_links.append(ExtLink(link_id=link_id_next,
                                     ext_node=l1cache_nodes[i*4],
                                     int_node=routers[i]))
            link_id_next += 1
            ext_links.append(ExtLink(link_id=link_id_next,
                                     ext_node=l1cache_nodes[i*4+1],
                                     int_node=routers[i]))
            link_id_next += 1
            ext_links.append(ExtLink(link_id=link_id_next,
                                     ext_node=l1cache_nodes[i*4+2],
                                     int_node=routers[i]))
            link_id_next += 1
            ext_links.append(ExtLink(link_id=link_id_next,
                                     ext_node=l1cache_nodes[i*4+3],
                                     int_node=routers[i]))
            link_id_next += 1
            ext_links.append(ExtLink(link_id=link_id_next,
                                     ext_node=l2cache_nodes[i],
                                     int_node=routers[i]))
            link_id_next += 1

        # The following 8 ID Routers for Mem controllers
        for i in range(4):
            ext_links.append(ExtLink(link_id=link_id_next,
                                     ext_node=dir_nodes[i],
                                     int_node=routers[i+16]))
            link_id_next += 1
            ext_links.append(ExtLink(link_id=link_id_next,
                                     ext_node=dir_nodes[i+4],
                                     int_node=routers[i+16]))
            link_id_next += 1
        for i in range(8,12):
            ext_links.append(ExtLink(link_id=link_id_next,
                                     ext_node=dir_nodes[i],
                                    int_node=routers[i+16]))
            link_id_next += 1
            ext_links.append(ExtLink(link_id=link_id_next,
                                     ext_node=dir_nodes[i+4],
                                     int_node=routers[i+16]))
            link_id_next += 1

        # The next links are for the DMA controllers.
        # Two of them (0 and 2) are contentrated in the same NIC
        ext_links.append(ExtLink(link_id=link_id_next,
                                 ext_node=dma_nodes[0],
                                 int_node=routers[24]))
        link_id_next += 1
        ext_links.append(ExtLink(link_id=link_id_next,
                                 ext_node=dma_nodes[2],
                                 int_node=routers[24]))
        link_id_next += 1
        ext_links.append(ExtLink(link_id=link_id_next,
                                 ext_node=dma_nodes[1],
                                 int_node=routers[31]))
        network.ext_links = ext_links

#        link_count = len(self.nodes)
#        int_links = [IntLink(link_id=(link_count+i),
#                             node_a=routers[i], node_b=xbar)
#                        for i in range(len(self.nodes))]
#        network.int_links = int_links
