/*
 * Copyright (c) 2015-2020 University of Cantabria
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Ivan Perez
 *
 */

#ifndef BOOKSIMCONSUMER_HPP
#define BOOKSIMCONSUMER_HPP

#include "mem/ruby/common/Consumer.hh"

#include "mem/ruby/network/MessageBuffer.hh"
#include <vector>


class BooksimSwitch;
class BooksimNetwork;


class BooksimConsumer : public Consumer
{
    public:
        static bool trigger_wakeup;
        BooksimConsumer(SwitchID sid, BooksimSwitch *sw, uint32_t virt_nets);
        ~BooksimConsumer();

        void init(BooksimNetwork *network_ptr);
        void wakeup();

        void addInPort(const std::vector<MessageBuffer*>& in);

        void setWrapper(BooksimNetwork * w) { m_wrapper_ptr = w; }

        void RegisterMessageBuffers(
                std::vector<std::vector<MessageBuffer*>> *in,
                std::vector<std::vector<MessageBuffer*>> *out);

        // XXX: Are these methods needed for the initilization of the Stats?
        void clearStats();
        void collateStats();
        void print(std::ostream& out) const;

    private:
        SwitchID m_switch_id;
        BooksimSwitch * const m_switch;

        std::vector<std::vector<MessageBuffer*> > m_in;
        std::vector<std::vector<MessageBuffer*> > m_out;
        uint32_t m_virtual_networks;

        BooksimNetwork * m_wrapper_ptr;

        int m_next_report_time;

        std::vector<int> m_pending_message_count;
};
#endif
