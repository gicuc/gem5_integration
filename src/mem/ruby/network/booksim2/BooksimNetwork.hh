/*
 * Copyright (c) 2015-2020 University of Cantabria
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Ivan Perez
 *
 */


#ifndef __MEM_RUBY_NETWORK_BOOKSIMNETWORK_HH__
#define __MEM_RUBY_NETWORK_BOOKSIMNETWORK_HH__

#include <iostream>
#include <vector>

#include "base/types.hh"
#include "mem/ruby/network/Network.hh"
#include "mem/ruby/network/booksim2/BooksimSwitch.hh"
#include "params/BooksimNetwork.hh"

#include "booksim_wrapper.hpp"

class NetDest;
class MessageBuffer;
class BooksimConsumer;

class BooksimNetwork : public Network
{
    struct BookSimMessage {
        BooksimSwitch * bs_switch;
        NodeID dest;
        int subnet;
        MsgPtr msg;
    };

    public:
        typedef BooksimNetworkParams Params;

        BooksimNetwork(const Params *p);
        ~BooksimNetwork();

        int GeneratePacket(int source, int dest, int size, int cl, long time) {
            return m_booksim_wrapper->GeneratePacket(source,
                                                     dest,
                                                     size,
                                                     cl,
                                                     time);
        }

        void RunCycles(int cycles);

        bool CheckInFlightPackets() {
            return m_booksim_wrapper->CheckInFlightPackets();
        }

        // Returns pid
        int RetirePacket() {
            Booksim::BooksimWrapper::RetiredPacket rp =
                                            m_booksim_wrapper->RetirePacket();
            if (rp.pid > -1) {
                setPacketStats(rp.plat, rp.hops, rp.ps);
            }
            return rp.pid;
        }

        void init();

        // TODO: add bypass utilization
        void setPacketStats(int p_lat, int hops, int packet_size) {
            total_lat += p_lat;
            total_hops += hops;
            total_flits += packet_size;
            total_packets++;
        }

        void incTotalCycles() {
            total_cycles++;
        }

        void printStats(std::ostream& out) const;
        void clearStats();
        void printConfig(std::ostream& out) const;
        void reset();
        void print(std::ostream& out) const;

        // returns the queue requested for the given component
        MessageBuffer* getToNetQueue(NodeID id,
                                     bool ordered,
                                     int network_num,
                                     std::string vnet_type);
        MessageBuffer* getFromNetQueue(NodeID id,
                                       bool ordered,
                                       int network_num,
                                       std::string vnet_type);

        int getNumNodes() { return m_nodes;}

        // Methods used by Topology to setup the network
        void makeExtOutLink(SwitchID src, NodeID dest, BasicLink* link,
                         const NetDest& routing_table_entry);
        void makeExtInLink(NodeID src, SwitchID dest, BasicLink* link,
                        const NetDest& routing_table_entry);
        void makeInternalLink(SwitchID src, SwitchID dest, BasicLink* link,
                              const NetDest& routing_table_entry,
                              PortDirection src_outport_dirn,
                              PortDirection dest_inport_dirn);

        void collateStats();
        void regStats();

        /*
         * Virtual functions for functionally reading and writing packets in
         * the network. Each network needs to implement these for functional
         * accesses to work correctly.
         */
        bool functionalRead(Packet* pkt){ return 0; }
        uint32_t functionalWrite(Packet* pkt){ return 0; }



        std::vector<std::vector<MessageBuffer*>>* GetInBuffers(){
            return &m_toNetQueues;
        }
        std::vector<std::vector<MessageBuffer*>>* GetOutBuffers(){
            return &m_fromNetQueues;
        }

        Stats::Scalar total_lat;
        Stats::Scalar total_hops;
        Stats::Scalar total_flits;
        Stats::Scalar total_packets;
        Stats::Scalar total_cycles;
        Stats::Formula avg_plat;
        Stats::Formula avg_throughput;
        Stats::Formula avg_hops;
        Stats::Formula avg_packet_size;

        MachineID getSwitchID(NodeID node);

        SwitchID TranslateSwitchID(NodeID ID){
            return m_switchID[ID];
        }

        BooksimSwitch* GetSwitchPtr(int ID){
            return m_switches[ID];
        }

        int GetNumSwitches(){return m_switches.size();}

        // XXX: Used by Booksim Consumer to avoid the execution of multiple
        // instances of Booksim's traffic manager
        Tick getLastWakeupCycle(){return m_wakeup_cycle;}
        void setLastWakeupCycle(Tick cycle){m_wakeup_cycle = cycle;}

        void putBookSimMessage(BooksimSwitch * switch_ptr,
                               int pid,
                               NodeID destID,
                               int subnet,
                               MsgPtr msg,
                               int packet_destination) {
            BookSimMessage message = {GetSwitchPtr(packet_destination),
                                      destID,
                                      subnet,
                                      msg};
            m_packet_map[pid] = message;
        }
        void enqueueBookSimMessage(int pid){
            BookSimMessage message = m_packet_map[pid];
            if (message.bs_switch) { // XXX: is this comp. necessary?
                message.bs_switch->EnqueueMessage(message.dest,
                                              message.subnet,
                                              message.msg);
            }
            m_packet_map.erase(pid);
        }

    private:
        std::vector<BooksimSwitch*> m_switches;
        int _number_switches; // For stat computation

        // Private copy constructor and assignment operator
        BooksimNetwork(const BooksimNetwork& obj);
        BooksimNetwork& operator=(const BooksimNetwork& obj);

        long m_wakeup_cycle;

        std::vector<MessageBuffer*> m_buffers_to_free;
        int m_buffer_size;

        std::vector<SwitchID> m_switchID;

        Booksim::BooksimWrapper * m_booksim_wrapper;

        // Store the packet ID in a dictionary to send
        // it to the destination buffer when reaching
        // destination
        std::map<int,BookSimMessage> m_packet_map;

};

std::ostream& operator<<(std::ostream& out, const BooksimNetwork& obj);

// ******************* Definitions *******************

// Output operator definition
extern inline std::ostream&
operator<<(std::ostream& out, const BooksimNetwork& obj)
{
    obj.print(out);
    out << std::flush;
    return out;
}

#endif
