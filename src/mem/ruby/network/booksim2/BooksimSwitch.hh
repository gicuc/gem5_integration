/*
 * Copyright (c) 2015-2020 University of Cantabria
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Ivan Perez
 *
 */

#ifndef __MEM_RUBY_NETWORK_BOOKSIM_SWITCH_HH__
#define __MEM_RUBY_NETWORK_BOOKSIM_SWITCH_HH__

#include <iostream>
#include <vector>

#include "mem/packet.hh"
#include "mem/ruby/slicc_interface/Message.hh"
#include "mem/ruby/common/TypeDefines.hh"
#include "mem/ruby/network/BasicRouter.hh"
#include "params/BooksimSwitch.hh"


class MessageBuffer;
class BooksimConsumer;
class NetDest;
class BooksimNetwork;

namespace BookSimNS {
    class gem5TrafficManager;
}

class BooksimSwitch : public BasicRouter
{
    public:
        typedef BooksimSwitchParams Params;
        BooksimSwitch(const Params* p);
        ~BooksimSwitch();

        void init();
        void addInPort(const std::vector<MessageBuffer*>& in);
        //XXX: Booksim uses SimpleLinks (see gem5/configs/Network.py), that's
        //     why addOutPort has bw_multipler.
        void addOutPort(const std::vector<MessageBuffer*>& out,
                        const NetDest& routing_table_entry,
                        Cycles link_latency,
                        int bw_multiplier);

        void resetStats();
        void collateStats();
        void regStats();

        void print(std::ostream &out) const;
        void init_net_ptr(BooksimNetwork* net_ptr) {
            m_wrapper_ptr = net_ptr;
        }

        void ReadMessage();
        void EnqueueMessage(int dest, int subnetwork, MsgPtr msg);
        //void EnqueueMessage(int pid);

    private:
        BooksimSwitch(const BooksimSwitch& obj);
        BooksimSwitch& operator=(const BooksimSwitch &obj);

        BooksimConsumer* m_booksim_consumer;
        BooksimNetwork* m_wrapper_ptr;

        // Input, output queues
        std::vector<std::vector<MessageBuffer*>> m_in_buffers;
        std::vector<std::vector<MessageBuffer*>> m_out_buffers;

        // Map of destinations with output port
        std::map<int,int> m_dest_port;

        // Round Robin
        int m_last_port;
};

inline std::ostream&
operator<<(std::ostream& out, const BooksimSwitch& obj)
{
    obj.print(out);
    out << std::flush;
    return out;
}

#endif // __MEM_RUBY_NETWORK_BOOKSIM_SWITCH_HH__
