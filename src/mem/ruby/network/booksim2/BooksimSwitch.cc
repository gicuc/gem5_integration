/*
 * Copyright (c) 2015-2020 University of Cantabria
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Ivan Perez
 *
 */


#include "mem/ruby/network/booksim2/BooksimSwitch.hh"

#include <numeric>

#include "base/cast.hh"
#include "base/stl_helpers.hh"
#include "mem/ruby/network/booksim2/BooksimConsumer.hh"
#include "mem/ruby/network/booksim2/BooksimNetwork.hh"

using namespace std;
using m5::stl_helpers::deletePointers;
using m5::stl_helpers::operator<<;

BooksimSwitch::BooksimSwitch(const Params *p) : BasicRouter(p)
{
    m_booksim_consumer = new BooksimConsumer(m_id, this, p->virt_nets);
    m_last_port = 0;
}

BooksimSwitch::~BooksimSwitch()
{
    delete m_booksim_consumer;
}

void
BooksimSwitch::init()
{
    BasicRouter::init();
    m_booksim_consumer->init(m_wrapper_ptr);
}

void
BooksimSwitch::addInPort(const vector<MessageBuffer*>& in)
{
    m_booksim_consumer->addInPort(in);
    m_in_buffers.push_back(in);
}

void
BooksimSwitch::addOutPort(const vector<MessageBuffer*>& out,
                          const NetDest& routing_table_entry,
                          Cycles link_latency,
                          int bw_multiplier)
{
    m_out_buffers.push_back(out);

    NetDest dest_table = routing_table_entry;

    // Create map of destinations.
    std::vector<NodeID> all_dest = dest_table.getAllDest();
    for (int dest_index = 0; dest_index < all_dest.size(); dest_index++) {
        m_dest_port[all_dest[dest_index]] = m_out_buffers.size()-1;
    }
}

void
BooksimSwitch::ReadMessage()
{
    Tick current_time = clockEdge();

    // XXX: BooksimSwitch gathers all the controller ports interconnected to the
    // BasicRouter and injects their messages by a single port in the
    // corresponding BookSim router. It's equivalent to external concentration.
    // This way we can map easier Ruby's topology in BookSim's one.
    // XXX: Round robin selection of the injection ports.
    if (m_in_buffers.size() > 0)
    {
        // Circular counter
        int port = (m_last_port + 1) % m_in_buffers.size(); 
        for (int i = 0; i < m_in_buffers.size(); ++i) {
            for (int vn = 0; vn < m_in_buffers[port].size(); ++vn) {
                if (m_in_buffers[port][vn] != nullptr) {
                    if (m_in_buffers[port][vn]->isReady(current_time)) {

                        //TODO: Check the occupancy of BookSim's injection
                        //      For the moment, use huge or infinite inj.
                        //      queues. The injection queue size should be 
                        //      defined in the injection queues of Ruby so it 
                        //      is not important.

                        // Get Message
                        MsgPtr msg_ptr = m_in_buffers[port][vn]->peekMsgPtr();

                        // Generate Booksim message if possible
                        Message* net_msg_ptr = msg_ptr.get();
                        NetDest msg_destinations =
                            net_msg_ptr->getDestination();
                        vector<NodeID> all_dest =
                            msg_destinations.getAllDest();

                        bool dequeue_msg = false;
                        //XXX: Dequeue INV messages with 0 destinations
                        //     (found them when using MOESI_CMP_directory)
                        if (all_dest.size() == 0) {
                            dequeue_msg = true;
                        }

                        //TODO: Read link width. Now hard-coded to 128 bits
                        int size = (int) ceil((double) m_wrapper_ptr->
                                MessageSizeType_to_int(net_msg_ptr->
                                getMessageSize())*8/128);

                        for (int dest_index = 0;
                             dest_index < all_dest.size();
                             dest_index++
                        ) {

                            MsgPtr new_msg_ptr = msg_ptr->clone();
                            NodeID destID = all_dest[dest_index];

                            Message *new_net_msg_ptr = new_msg_ptr.get();

                            NetDest personal_dest;
                            if (all_dest.size() > 1) {
                                for (int m = 0; m < (int) MachineType_NUM; m++)
                                {
                                    if ((destID >=
                                        MachineType_base_number(
                                            (MachineType) m)) &&
                                            destID < MachineType_base_number(
                                                (MachineType) (m+1))
                                       ) {
                                        // calculating the NetDest associated
                                        // with this destID
                                        personal_dest.clear();
                                        personal_dest.add((MachineID) {
                                                (MachineType) m, (destID -
                                                MachineType_base_number(
                                                    (MachineType) m))});
                                        new_net_msg_ptr->getDestination() =
                                            personal_dest;
                                        break;
                                    }
                                }
                            }

                            int packet_destination = all_dest[dest_index];
                            packet_destination = m_wrapper_ptr->
                                TranslateSwitchID(packet_destination);

                            //TODO: set packet class base on the selection
                            //      between subnetworks or virtual networks
                            //      For the moment we assume virtual networks
                            int packet_class = vn;

                            // Injection cycle for BookSim.
                            Cycles ic = curCycle() - ticksToCycles(
                                                        msg_ptr->getTime()
                                                     );
                            int pid = m_wrapper_ptr->GeneratePacket(
                                                        m_id,
                                                        packet_destination,
                                                        size,
                                                        packet_class,
                                                        ic
                                                     );

                            if (-1 < pid) {
                                msg_destinations.removeNetDest(personal_dest);

                                net_msg_ptr->
                                    getDestination().removeNetDest(
                                            personal_dest
                                            );

                                m_wrapper_ptr->putBookSimMessage(this,
                                        pid,
                                        destID,
                                        vn,
                                        new_msg_ptr,
                                        packet_destination);

                                dequeue_msg = true;
                            }
                        }

                        if (dequeue_msg) {
                            // Dequeue message
                            m_in_buffers[port][vn]->dequeue(current_time);
                            // Update round robin
                            m_last_port = port;
                        }
                    }
                }
            }
            // Circular counter
            port = (port+1) % m_in_buffers.size();
        }
    }
}

void
BooksimSwitch::EnqueueMessage(int dest, int subnet, MsgPtr msg)
{
    Tick current_time = clockEdge();
    int dest_port = m_dest_port[dest];
    m_out_buffers[dest_port][subnet]->enqueue(msg,
                                              current_time,
                                              cyclesToTicks(Cycles(1)));
}

void
BooksimSwitch::regStats()
{
    BasicRouter::regStats();
}

void
BooksimSwitch::resetStats()
{
    m_booksim_consumer->clearStats();
}

void
BooksimSwitch::collateStats()
{
    m_booksim_consumer->collateStats();
}

void
BooksimSwitch::print(std::ostream& out) const
{
    out << "[BooksimSwitch " << m_id << "]";
}

BooksimSwitch * BooksimSwitchParams::create()
{
    return new BooksimSwitch(this);
}
