/*
 * Copyright (c) 2015-2020 University of Cantabria
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Ivan Perez
 *
 */



#include <numeric>

#include "base/cast.hh"
#include "base/stl_helpers.hh"

#include "mem/ruby/network/booksim2/BooksimNetwork.hh"

#include "mem/ruby/network/booksim2/BooksimSwitch.hh"
#include "mem/ruby/network/simple/SimpleLink.hh"
#include "mem/ruby/profiler/Profiler.hh"

using m5::stl_helpers::deletePointers;

BooksimNetwork::BooksimNetwork(const Params *p)
    : Network(p)
{
    m_wakeup_cycle = 0;

    m_switchID.resize(m_nodes);

    // record the routers
    for (std::vector<BasicRouter*>::const_iterator i = p->routers.begin();
            i != p->routers.end();
            ++i) {
        BooksimSwitch* s = safe_cast<BooksimSwitch*>(*i);
        m_switches.push_back(s);
        s->init_net_ptr(this);
    }

    _number_switches = m_switches.size();

    m_booksim_wrapper = new Booksim::BooksimWrapper(p->booksim_config);
}

void
BooksimNetwork::init()
{
    Network::init();
    assert(m_topology_ptr != NULL);
    m_topology_ptr->createLinks(this);
}

BooksimNetwork::~BooksimNetwork()
{
    for (int i = 0; i < m_nodes; i++) {
        deletePointers(m_toNetQueues[i]);
        deletePointers(m_fromNetQueues[i]);
    }
}

BooksimNetwork*
BooksimNetworkParams::create()
{
    return new BooksimNetwork(this);
}

void
BooksimNetwork::printStats(std::ostream& out) const
{
    //TODO: print something?
}

void
BooksimNetwork::clearStats()
{
}

void
BooksimNetwork::printConfig(std::ostream& out) const
{
    //TODO: print something?
}

void
BooksimNetwork::reset()
{
    for (int node = 0; node < m_nodes; node++) {
        for (int j = 0; j < m_virtual_networks; j++) {
            m_toNetQueues[node][j]->clear();
            m_fromNetQueues[node][j]->clear();
        }
    }
}

void
BooksimNetwork::print(std::ostream& out) const
{
    out << "[BooksimNetwork]";
}

MachineID
BooksimNetwork::getSwitchID(NodeID node)
{
    for (MachineType m = MachineType_FIRST; m < MachineType_NUM; ++m) {
        int num_machines = MachineType_base_count(m);
        if (node < num_machines) {
            MachineID mid = {m, node};
            return mid;
        }
        else {
            node = node - num_machines;
        }
    }
    MachineType merror = MachineType_FIRST;
    MachineID error = {merror, node};
    return error;
}

/*
 * This function creates a link from the Network to a NI.
 * It creates a Network Link from a Router to the NI and
 * a Credit Link from NI to the Router
*/

void
BooksimNetwork::makeExtOutLink(SwitchID src, NodeID dest, BasicLink* link,
                             const NetDest& routing_table_entry)
{
    assert(dest < m_nodes);
    assert(src < m_switches.size());
    assert(m_switches[src] != NULL);

    SimpleExtLink* simple_link = safe_cast<SimpleExtLink*>(link);
    m_switches[src]->addOutPort(m_fromNetQueues[dest],
                                routing_table_entry,
                                simple_link->m_latency,
                                simple_link->m_bw_multiplier);
}

/*
 * This function creates a link from the Network Interface (NI)
 * into the Network.
 * It creates a Network Link from the NI to a Router and a Credit Link from
 * the Router to the NI
*/

void
BooksimNetwork::makeExtInLink(NodeID src, SwitchID dest, BasicLink* link,
                            const NetDest& routing_table_entry)
{
    assert(src < m_nodes);
    m_switchID[src] = dest;
    m_switches[dest]->addInPort(m_toNetQueues[src]);
}

/*
 * This function creates an internal network link between two routers.
 * We don't use it in BookSim as it uses its internal links
*/

void
BooksimNetwork::makeInternalLink(SwitchID src, SwitchID dest, BasicLink* link,
                                 const NetDest& routing_table_entry,
                                 PortDirection src_outport_dirn,
                                 PortDirection dst_inport_dirn)
{
}


void
BooksimNetwork::RunCycles(int cycles)
{
    for (int i = 0; i < m_switches.size(); ++i) {
        m_switches[i]->ReadMessage();
    }

    m_booksim_wrapper->RunCycles(cycles);
}




void
BooksimNetwork::regStats()
{
    Network::regStats();

    total_lat
        .name(name() + ".total_plat")
        .desc("Total packet latency")
        ;
    total_packets
        .name(name() + ".total_packets")
        .desc("Total packets")
        ;
    total_hops
        .name(name() + ".total_hops")
        .desc("Total hops")
        ;
    total_flits
        .name(name() + ".total_flits")
        .desc("Total flits")
        ;
    total_cycles
        .name(name() + ".total_cycles")
        .desc("Total cycles")
        ;
    avg_plat
        .name(name() + ".avg_plat")
        .desc("Avg. packet latency")
        ;
    avg_throughput
        .name(name() + ".avg_throughput")
        .desc("Avg. throughput")
        ;
    avg_hops
        .name(name() + ".avg_hops")
        .desc("Avg. number of hops")
        ;
    avg_packet_size
        .name(name() + ".avg_packet_size")
        .desc("Avg. packet size")
        ;
    avg_plat = total_lat / total_packets;
    avg_throughput = total_flits / total_cycles / _number_switches;
    avg_hops = total_hops / total_packets;
    avg_packet_size = total_flits / total_packets;
}

void
BooksimNetwork::collateStats()
{
    for (int i = 0; i < m_switches.size(); i++) {
        m_switches[i]->collateStats();
    }
}

