/*
 * Copyright (c) 2015-2020 University of Cantabria
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Ivan Perez
 *
 */

#include <sstream>
#include <string>

#include "mem/ruby/network/booksim2/BooksimConsumer.hh"
#include "mem/ruby/network/booksim2/BooksimNetwork.hh"
#include "mem/ruby/network/booksim2/BooksimSwitch.hh"

BooksimConsumer::BooksimConsumer(SwitchID sid,
                                 BooksimSwitch *sw,
                                 uint32_t virt_nets) 
    : Consumer(sw), m_switch_id(sid), m_switch(sw)
{
    m_virtual_networks = virt_nets;
    // FIXME: hard-coded, it should be a parameter (and it is unused)
    m_next_report_time = 500000;
}

BooksimConsumer::~BooksimConsumer()
{
}

void
BooksimConsumer::init(BooksimNetwork* network_ptr)
{
    m_wrapper_ptr = network_ptr;

    for (int i = 0;i < m_virtual_networks;++i) {
        m_pending_message_count.push_back(0);
    }
}

void
BooksimConsumer::addInPort(const std::vector<MessageBuffer*>& in)
{
    NodeID port = m_in.size();
    m_in.push_back(in);

    // TODO: check if number of nodes is valid for the booksim topology
    // What is the best way of checking the number of nodes in BookSim?
    // The number of gem5 nodes is the number of Switches, not the aggregated
    // number of injection ports.

    for (int i = 0; i < in.size(); ++i) {
        if (in[i] != nullptr) {
            in[i]->setConsumer(this);
            in[i]->setIncomingLink(port);
            in[i]->setVnet(i);
        }
    }
}

void
BooksimConsumer::wakeup()
{
    Tick current_cycle = m_switch->clockEdge();
    if (m_wrapper_ptr->getLastWakeupCycle() <= current_cycle) {
        m_wrapper_ptr->incTotalCycles();

        // Execute next BookSim cycle
        m_wrapper_ptr->RunCycles(1);
        int pid = -1;

        // Check and retire packets in BookSim ejection queues.
        do {
            pid = m_wrapper_ptr->RetirePacket();
            if (pid > -1) {
                m_wrapper_ptr->enqueueBookSimMessage(pid);
            }
        } while (pid != -1);

        // Schedule event for next cycle if there are flits in the NoC
        if (m_wrapper_ptr->CheckInFlightPackets()) {
            scheduleEvent(Cycles(1));
        }

        // Update last wakeup cycle for statistics
        m_wrapper_ptr->setLastWakeupCycle(current_cycle+1);
    }
}

void
BooksimConsumer::RegisterMessageBuffers(
        std::vector<std::vector<MessageBuffer*>> *in,
        std::vector<std::vector<MessageBuffer*>> *out)
{
}

void
BooksimConsumer::clearStats()
{
    // TODO: create a method to clear Booksim internal stats
}

//XXX: Can I remove this method?
void
BooksimConsumer::collateStats()
{
}

void
BooksimConsumer::print(std::ostream& out) const
{
    out << "[BooksimConsumer " << m_switch_id << "]";
}
