# gem5 integration

This repository provides the code and instructions to integrate BookSim in
gem5.

The most recent gem5 commit this source code has been tested is:
```git
commit 1db7ecb4ce46e4ea525e6564032b4486c72c1c41
Author: Bobby R. Bruce <bbruce@ucdavis.edu>
Date:   Mon Jan 27 18:43:32 2020 -0800

    misc: Updated CONTRIBUTING.md to discuss releases and hotfixes

    A new section in CONTRIBUTING.md has been added to discuss the
    proceedure for how releases are carried out, as well as hotfixes.

    Jira: https://gem5.atlassian.net/browse/GEM5-297
    Change-Id: I49e7d6e41e8a6d5387c839eb26263e86dd52c294
    Reviewed-on: https://gem5-review.googlesource.com/c/public/gem5/+/24843
    Reviewed-by: Daniel Carvalho <odanrc@yahoo.com.br>
    Reviewed-by: Jason Lowe-Power <jason@lowepower.com>
    Maintainer: Bobby R. Bruce <bbruce@ucdavis.edu>
    Tested-by: kokoro <noreply+kokoro@google.com>
```
## Step by step instructions

#### 1. Clone gem5 repository

```bash
git clone https://gem5.googlesource.com/public/gem5
```

#### 2. Clone BST repository

```bash
git clone https://bitbucket.org/gicuc/bst.git
```

#### 3. Move booksim2 wrapper code in gem5 src. (This step may require changes in
the code.)

```bash
mv <bst_dir>/gem5_integration/src/mem/ruby/network/booksim2 \
   <gem5_dir>/src/mem/ruby/network/
```

#### 4. Edit `<gem5_dir>/src/mem/ruby/network/booksim2/Sconscript` to set LIBPATH
(line 39) for your gem5 path.

#### 5. Add Booksim cmd options applying differences to config/network/Network.py

```bash
vimdiff <bst_dir>/gem5_integration/configs/network/Network.py \
		<gem5_dir>/configs/network/Network.py
```

#### 6. (Optional) Move Ruby topologies to config directory

```bash
mv <bst_dir>/gem5_integration/configs/topologies/* \
   <gem5_dir>/configs/topologies/
```

#### 7. Compile BookSim as a shared-library

```bash
cd booksim-unican/booksim2
make lib
```

#### 8. Copy library to gem5 root dir

```bash
cp booksim-unican/booksim2/libbooksim.so <gem5_dir>/
```

#### 9. Compile gem5. The next command compiles gem5 for a quick test using
synthetic traffic.

```bash
scons build/Garnet_standalone/gem5.fast -j 4
```

#### 10. Run a test:
The following examples use booksim configurations hosted in `unican-util`.
In order to copy the commands copy `unican-util` to gem5 dir:

```
cp <bst_dir>/gem5_integration/unican-util \
   <gem5_dir>
```

* 5-stage router (BookSim's IQ router)

```bash
LD_LIBRARY_PATH=. ./build/Garnet_standalone/gem5.fast configs/example/garnet_synth_traffic.py  \
                     --num-cpus=64 \
                     --num-dirs=64 \
                     --network=booksim2 \
                     --topology=Mesh_XY \
                     --mesh-rows=8  \
                     --sim-cycles=1000 \
                     --synthetic=uniform_random \
                     --injectionrate=0.01 \
                     --booksim-config=unican-util/booksim_configs/examples/mesh_8x8_virtual_networks.cfg

grep plat m5out/stats.txt
system.ruby.network.avg_plat                36.415842                       # Avg. packet latency
system.ruby.network.total_plat                  22068                       # Total packet latency
```

* Single-hop bypass router (NEBB-Hybrid)

```bash
LD_LIBRARY_PATH=. ./build/Garnet_standalone/gem5.fast configs/example/garnet_synth_traffic.py  \
                     --num-cpus=64 \
                     --num-dirs=64 \
                     --network=booksim2 \
                     --topology=Mesh_XY \
                     --mesh-rows=8  \
                     --sim-cycles=1000 \
                     --synthetic=uniform_random \
                     --injectionrate=0.01 \
                     --booksim-config=unican-util/booksim_configs/examples/mesh_8x8_hybrid.cfg

grep plat m5out/stats.txt
system.ruby.network.avg_plat                19.065359                       # Avg. packet latency
system.ruby.network.total_plat                  11668                       # Total packet latency
```

> TODO: compilation of BookSim as static library
